package fr.arkadgames.moderator;

import org.bukkit.plugin.java.JavaPlugin;

import fr.arkadgames.moderator.commands.NameCommand;
import fr.arkadgames.moderator.commands.ProfileCommand;
import fr.arkadgames.moderator.commands.SkinCommand;

public class Moderator extends JavaPlugin {
	
	@Override
	public void onEnable() {
		getCommand("name").setExecutor(new NameCommand());
		//getCommand("uuid").setExecutor(new UuidCommand());
		getCommand("skin").setExecutor(new SkinCommand());
		getCommand("profile").setExecutor(new ProfileCommand());
	}

}
