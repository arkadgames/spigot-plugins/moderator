package fr.arkadgames.moderator.commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import fr.mrcubee.plugin.spigot.utils.player.PlayerProfileEditor;
import fr.mrcubee.plugin.utils.MinecraftValues;
import fr.mrcubee.utils.UUIDConverter;
import fr.mrcubee.utils.mojang.MojangProfile;

public class UuidCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player;
		String uuid;
		
		if (!(sender instanceof Player) && args.length < 2)
			return false;
		if (args.length < 2)
			player = (CraftPlayer) sender;
		else
			player = (CraftPlayer) Bukkit.getPlayer(args[0]);
		if (player == null) {
			sender.sendMessage(ChatColor.RED + "The player does not exist or is not connected.");
			return false;
		}
		if (args.length < 1) {
			sender.sendMessage(ChatColor.BLUE + "/nick <name/uuid>");
			sender.sendMessage(ChatColor.BLUE + "/nick <player> <name/uuid>");
			return true;
		}
		if (args.length < 2)
			uuid = args[0];
		else
			uuid = args[1];
		if (uuid.length() != MinecraftValues.UUID_FULL_LENGTH)
			return false;
		PlayerProfileEditor.setUUID(player, UUID.fromString(uuid));
		PlayerProfileEditor.updatePlayer(player, false);
		return true;
	}

}
