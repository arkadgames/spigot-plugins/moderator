package fr.arkadgames.moderator.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import fr.mrcubee.plugin.spigot.utils.player.PlayerProfileEditor;
import fr.mrcubee.utils.mojang.MojangProfile;

public class ProfileCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player;
		MojangProfile mojangProfile;
		
		if ((!(sender instanceof Player) && args.length < 2) || !sender.hasPermission("moderator.profile"))
			return false;
		if (args.length < 2)
			player = (CraftPlayer) sender;
		else
			player = (CraftPlayer) Bukkit.getPlayer(args[0]);
		if (player == null) {
			sender.sendMessage(ChatColor.RED + "The player does not exist or is not connected.");
			return false;
		}
		if (args.length < 1) {
			sender.sendMessage(ChatColor.BLUE + "/profile <name/uuid>");
			sender.sendMessage(ChatColor.BLUE + "/profile <player> <name/uuid>");
			return true;
		}
		if (args.length < 2)
			mojangProfile = MojangProfile.getMojangProfile(args[0]);
		else
			mojangProfile = MojangProfile.getMojangProfile(args[1]);
		if (mojangProfile == null) {
			sender.sendMessage(ChatColor.RED + "The player's skin does not exist.");
			return false;
		}
		PlayerProfileEditor.setProfile(player, mojangProfile, false);
		player.setCustomName(null);
		return true;
	}

}
